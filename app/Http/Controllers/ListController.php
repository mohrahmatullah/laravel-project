<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\View;
use App\Http\Requests\createRequest;
use App\Http\Requests\updateRequest;
use Session;
use Illuminate\Pagination\LengthAwarePaginator;

class ListController extends Controller
{
    public function index(Request $request)
    {
        $response = Http::post('http://maimaid.id:8002/user/read');
        $data = $this->getPaginator($response->json()['data']['rows'], $request);
        return view('index',compact('data'));
    }

    public function add(){
        return view('create');
    }

    public function create( createRequest $request ){
        if($request->input('agree') == 1){
            $response = Http::post('http://maimaid.id:8002/user/create', [
                'fullname' => $request->input('fullname'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'gender' => $request->input('gender'),
                'dob' => date('Y-m-d', strtotime($request->input('dob')))
            ]);
            $data = $response->json();
            $alert_toast = 
            [
                'title' => 'Operation Successful : ',
                'text'  => 'Successfully Added.',
                'type'  => 'success',
            ];
            Session::flash('alert_toast', $alert_toast);
            return redirect()->route('home');
        }else{
            $alert_toast = 
            [
                'title' => 'Operation Error : ',
                'text'  => 'Centang dulu agree',
                'type'  => 'warning',
            ];
            Session::flash('alert_toast', $alert_toast);
            return redirect()->back();
        }
        
    }

    public function view( $id ){
        $response = Http::post('http://maimaid.id:8002/user/view', [
                    'id' => $id
                ]);
        $data = $response->json()['data'];

        return view('edit', compact('data'));
    }

    public function update( updateRequest $request ){
        if($request->input('agree') == 1){
            if(!empty($request->input('password'))){
                $response = Http::post('http://maimaid.id:8002/user/update', [
                        'id' => $request->input('id'),
                        'fullname' => $request->input('fullname'),
                        'email' => $request->input('email'),
                        'password' => $request->input('password'),
                        'gender' => $request->input('gender'),
                        'dob' => date('Y-m-d', strtotime($request->input('dob')))
                    ]);
      
                $data = $response->json();
                $alert_toast = 
                [
                    'title' => 'Operation Successful : ',
                    'text'  => 'Successfully Updated.',
                    'type'  => 'success',
                ];
                Session::flash('alert_toast', $alert_toast);
                return redirect()->route('home');
            }else{
                $response = Http::post('http://maimaid.id:8002/user/update', [
                            'id' => $request->input('id'),
                            'fullname' => $request->input('fullname'),
                            'email' => $request->input('email'),
                            'gender' => $request->input('gender'),
                            'dob' => date('Y-m-d', strtotime($request->input('dob')))
                        ]);
          
                $data = $response->json();
                $alert_toast = 
                [
                    'title' => 'Operation Successful : ',
                    'text'  => 'Successfully Updated.',
                    'type'  => 'success',
                ];
                Session::flash('alert_toast', $alert_toast);
                return redirect()->route('home');
            }
            
        }else{
            $alert_toast = 
            [
                'title' => 'Operation Error : ',
                'text'  => 'Centang dulu agree',
                'type'  => 'warning',
            ];
            Session::flash('alert_toast', $alert_toast);
            return redirect()->back();
        }
        
    }

    public function lihat( $id ){
        $response = Http::post('http://maimaid.id:8002/user/view', [
                    'id' => $id
                ]);
        $data = $response->json()['data'];

        return view('view', compact('data'));
    }

    public function getPaginator($items, $request)
    {
        $total = count($items); // total count of the set, this is necessary so the paginator will know the total pages to display
        $page = $request->page ? $request->page : 1; // get current page from the request, first page is null
        $perPage = 10; // how many items you want to display per page?
        $offset = ($page - 1) * $perPage; // get the offset, how many items need to be "skipped" on this page

        $items = array_slice($items, $offset, $perPage); // the array that we actually pass to the paginator is sliced

        return new LengthAwarePaginator($items, $total, $perPage, $page, [
            'path' => $request->url(),
            'query' => $request->query()
        ]);
    }
}
